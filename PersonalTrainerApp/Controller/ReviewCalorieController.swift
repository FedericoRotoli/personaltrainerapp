//
//  ReviewCalorieController.swift
//  PersonalTrainerApp
//
//  Created by Federico Rotoli on 04/03/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit

class ReviewCalorieController: UIViewController {
    
    var Kcal: Double = 0.0
    var KcalWL = 0.0
    var KcalWI = 0.0
    
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var kcalLabel: UILabel!
    @IBOutlet weak var kcalWL: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var viewContainerWL: UIView!
    @IBOutlet weak var viewControllerWI: UIView!
    @IBOutlet weak var kcalWI: UILabel!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        doneButton.layer.cornerRadius = 20
        viewContainer.layer.borderWidth = 2
        viewContainer.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        viewContainer.layer.cornerRadius = 10
        viewContainerWL.layer.borderWidth = 2
        viewContainerWL.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        viewContainerWL.layer.cornerRadius = 10
        viewControllerWI.layer.borderWidth = 2
        viewControllerWI.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        viewControllerWI.layer.cornerRadius = 10
        let kcalString = String(format: "%.0f", Kcal)
        kcalLabel.text = "\(kcalString) Kcal/day"
        KcalWL = Kcal * 0.79
        KcalWI = Kcal * 1.18
        let kcalStrWI = String(format: "%.0f", KcalWI)
        let kcalStrWL = String(format: "%.0f", KcalWL)
        kcalWI.text = "\(kcalStrWI) Kcal/day"
        kcalWL.text = "\(kcalStrWL) Kcal/day"
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        navigationController?.popViewControllers(viewsToPop: 2)
    }
    
    @IBAction func copy1(_ sender: Any) {
        UIPasteboard.general.string = kcalLabel.text
        
        let alert = UIAlertController(title: nil, message: "Succesfully copied the value in the clipboard!", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    @IBAction func copy2(_ sender: Any) {
        UIPasteboard.general.string = kcalWL.text
        
        let alert = UIAlertController(title: nil, message: "Succesfully copied the value in the clipboard!", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    @IBAction func copy3(_ sender: Any) {
        UIPasteboard.general.string = kcalWI.text
        
        let alert = UIAlertController(title: nil, message: "Succesfully copied the value in the clipboard!", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
}
