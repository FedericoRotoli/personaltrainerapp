//
//  ToolsController.swift
//  PersonalTrainerApp
//
//  Created by Federico Rotoli on 17/02/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit

class ToolsController: UIViewController {
    
    @IBOutlet weak var calorieButton: UIButton!
    @IBOutlet weak var macroButton: UIButton!
    @IBOutlet weak var bmiButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
    }
    
    func setup() {
        calorieButton.layer.cornerRadius = 10
        macroButton.layer.cornerRadius = 10
        bmiButton.layer.cornerRadius = 10
        navigationController?.navigationBar.prefersLargeTitles = true
        
        calorieButton.layer.shadowColor = UIColor.black.cgColor
        calorieButton.layer.shadowOffset = CGSize(width: 0, height: 3.0)
        calorieButton.layer.shadowRadius = 4.0
        calorieButton.layer.shadowOpacity = 0.3
        calorieButton.layer.masksToBounds = false
        calorieButton.layer.shadowPath = UIBezierPath(roundedRect: calorieButton.bounds, cornerRadius: 10).cgPath
        macroButton.layer.shadowColor = UIColor.black.cgColor
        macroButton.layer.shadowOffset = CGSize(width: 0, height: 3.0)
        macroButton.layer.shadowRadius = 4.0
        macroButton.layer.shadowOpacity = 0.3
        macroButton.layer.masksToBounds = false
        macroButton.layer.shadowPath = UIBezierPath(roundedRect: macroButton.bounds, cornerRadius: 10).cgPath
        bmiButton.layer.shadowColor = UIColor.black.cgColor
        bmiButton.layer.shadowOffset = CGSize(width: 0, height: 3.0)
        bmiButton.layer.shadowRadius = 4.0
        bmiButton.layer.shadowOpacity = 0.3
        bmiButton.layer.masksToBounds = false
        bmiButton.layer.shadowPath = UIBezierPath(roundedRect: bmiButton.bounds, cornerRadius: 10).cgPath
        
    }
}
