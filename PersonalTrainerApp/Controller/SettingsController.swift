//
//  SettingsController.swift
//  PersonalTrainerApp
//
//  Created by Federico Rotoli on 17/02/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit

class SettingsController: UITableViewController {
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var profileImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = profileImage.bounds.width / 2
        
        if let image = defaults.imageForKey(key: "profileImage") {
            profileImage.image = image
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
    }
}
