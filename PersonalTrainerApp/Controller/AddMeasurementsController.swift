//
//  AddMeasurementsController.swift
//  PersonalTrainerApp
//
//  Created by Luciano Amoroso on 02/03/2020.
//  Copyright © 2020 Luciano Amoroso. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class AddMeasurementsController: UIViewController {
 
    @IBOutlet weak var chest: UITextField!
    @IBOutlet weak var hipBone: UITextField!
    @IBOutlet weak var hips: UITextField!
    @IBOutlet weak var leg: UITextField!
    @IBOutlet weak var shoulders: UITextField!
    @IBOutlet weak var bicheps: UITextField!
    @IBOutlet weak var segSel: UISegmentedControl!
    
    override func viewDidLoad() {
    super.viewDidLoad()
        
    }
    @IBAction func doneBtn(_ sender: Any) {
        
        
        if (chest.text == "" || hipBone.text == "" || hips.text == "" ) {
                          let alert = UIAlertController(title: "Warning!", message: "Please, compile each form!", preferredStyle: .alert)

                          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

                          self.present(alert, animated: true)
               } else {
               
                  let appDelegate = UIApplication.shared.delegate as! AppDelegate
                      let context = appDelegate.persistentContainer.viewContext
                      let entity = NSEntityDescription.entity(forEntityName: "Measurements", in: context)
                      let item = NSManagedObject(entity: entity!, insertInto: context)

             if (segSel.selectedSegmentIndex == 0) {
                         let chests = Int(chest.text! ?? "") ?? 0
                          item.setValue(chests, forKey: "chestStart")
                         let hipb = Int(hips.text! ?? "") ?? 0
                          item.setValue(hipb, forKey: "hipsStart")
                         let hipbo = Int(hipBone.text! ?? "") ?? 0
                          item.setValue(hipbo, forKey: "hipboneStart")
                         let legs = Int(leg.text! ?? "") ?? 0
                          item.setValue(hipbo, forKey: "legStart")
                         let sht = Int(shoulders.text! ?? "") ?? 0
                          item.setValue(sht, forKey: "shouldersStart")
             } else {
                                       let chests = Int(chest.text! ?? "") ?? 0
                                        item.setValue(chests, forKey: "chestEnd")
                                       let hipb = Int(hips.text! ?? "") ?? 0
                                        item.setValue(hipb, forKey: "hipsEnd")
                                       let hipbo = Int(hipBone.text! ?? "") ?? 0
                                        item.setValue(hipbo, forKey: "hipboneEnd")
                                       let legs = Int(leg.text! ?? "") ?? 0
                                        item.setValue(hipbo, forKey: "legEnd")
                                       let sht = Int(shoulders.text! ?? "") ?? 0
                                        item.setValue(sht, forKey: "shouldersEnd")
            }

                         do {
                            try context.save()
                           } catch {
                            print("Failed saving")
                         }
                         dismiss(animated: true, completion: {
                               NotificationCenter.default.post(name: Notification.Name.Action.reload2, object: nil)
                         })
           }
           }


        
    }

