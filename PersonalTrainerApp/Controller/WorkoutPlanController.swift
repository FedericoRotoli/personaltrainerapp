//
//  WorkoutPlanController.swift
//  PersonalTrainerApp
//
//  Created by Federico Rotoli on 23/02/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit

class WorkoutPlanController: UIViewController {
    
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    
    @IBOutlet weak var weekLabel: UILabel!
    @IBOutlet weak var myCollection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewWillLayoutSubviews()
        setupCollectionView()
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupCollectionViewItemSize()
    }
    
    private func setupCollectionView(){
        myCollection.delegate = self
        myCollection.dataSource = self
        let nib = UINib(nibName: "WorkoutPlanCell", bundle: nil)
        myCollection.register(nib, forCellWithReuseIdentifier: "cellIdentifier")
    }
    
    private func setupCollectionViewItemSize() {
        if collectionViewFlowLayout == nil {
            let lineSpacing: CGFloat = 0
            let interItemSpacing: CGFloat = 0
            
            let width = 414
            let height = 725
            
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            
            collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            collectionViewFlowLayout.scrollDirection = .horizontal
            collectionViewFlowLayout.minimumLineSpacing = lineSpacing
            collectionViewFlowLayout.minimumInteritemSpacing = interItemSpacing
            
            myCollection.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
        }
    }
}

extension WorkoutPlanController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
        return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdentifier", for: indexPath) as! WorkoutPlanCell
    
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let value = scrollView.contentOffset.x
        let index = value-view.frame.width
        
        if index < -207 {
            weekLabel.text = "Day 1"
        } else if index >= -207 && index <= 207 {
            weekLabel.text = "Day 2"
        } else if index > 207 && index < 621 {
            weekLabel.text = "Day 3"
        } else if index >= 621 && index <= 1035 {
            weekLabel.text = "Day 4"
        } else if index > 1035 && index <= 1449 {
            weekLabel.text = "Day 5"
        }
        
    }
}
