//
//  CalorieController.swift
//  PersonalTrainerApp
//
//  Created by Giovanni Carfora on 17/02/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit

class CalorieController: UIViewController {
    
    private var BMMan: Double?
    private var BMWoman: Double?
    private var KcalMan: Double?
    private var KcalWoman: Double?
    var Kcal: Double?
    private var european: Bool = true
    private var labelStr: String?
    
    
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLAbel: UILabel!
    @IBOutlet weak var formContainer: UIView!
    @IBOutlet weak var europeanButton: UIButton!
    @IBOutlet weak var americanButton: UIButton!
    @IBOutlet weak var amLabel: UILabel!
    @IBOutlet weak var euLabel: UILabel!
    @IBOutlet weak var age: UITextField!
    @IBOutlet weak var height: UITextField!
    @IBOutlet weak var weight: UITextField!
    @IBOutlet weak var dailyActivity: UISegmentedControl!
    @IBOutlet weak var workout: UISegmentedControl!
    @IBOutlet weak var gender: UISegmentedControl!
    @IBOutlet weak var calculateBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        self.tabBarController?.tabBar.isHidden = true

        age.addDoneCancelToolbar()
        weight.addDoneCancelToolbar()
        height.addDoneCancelToolbar()
        age.delegate = self
        weight.delegate = self
        height.delegate = self
        
        self.tabBarController?.tabBar.isHidden = true
        calculateBtn.layer.cornerRadius = 20
        
        formContainer.layer.borderWidth = 2
        formContainer.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        formContainer.layer.cornerRadius = 20
        
        workout.selectedSegmentTintColor(.white)
        dailyActivity.selectedSegmentTintColor(.white)
        gender.selectedSegmentTintColor(.white)
        
        americanButton.layer.borderWidth = 2
        americanButton.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        americanButton.layer.cornerRadius = 20
        europeanButton.layer.borderWidth = 2
        europeanButton.layer.borderColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1).cgColor
        europeanButton.backgroundColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)
        euLabel.textColor = UIColor.white
        
        europeanButton.layer.cornerRadius = 20
        



    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.tabBarController?.tabBar.isHidden = true
     }
    
    @IBAction func euSystemAction(_ sender: Any) {
//        if !european {
//            let generator = UIImpactFeedbackGenerator(style: .medium)
//             generator.impactOccurred()
//        }

        
        europeanButton.layer.borderColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1).cgColor
        europeanButton.backgroundColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)
        americanButton.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.2).cgColor
        americanButton.backgroundColor = .clear
        amLabel.textColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)
        euLabel.textColor = UIColor.white
        weightLAbel.text = "Weight (in kg)"
        heightLabel.text = "Height (in cm)"
        european = true
    }

    @IBAction func americaSystemAction(_ sender: Any) {
//        if european {
//            let generator = UIImpactFeedbackGenerator(style: .medium)
//             generator.impactOccurred()
//        }
        
        americanButton.layer.borderColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1).cgColor
        americanButton.backgroundColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)
        europeanButton.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.2).cgColor
        europeanButton.backgroundColor = .clear
        
        euLabel.textColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)
        
        amLabel.textColor = UIColor.white
        weightLAbel.text = "Weight (in lbs)"
        heightLabel.text = "Height (in ft)"
        european = false
    }
    
    func calculateBM() {
    
    let age1 = Double(age.text ?? "") ?? 0
    let height1 = height.text
    let newHeight = height1?.replacingOccurrences(of: ",", with: ".")
    var finalHeight = Double(newHeight ?? "") ?? 0
    
    let weight1 = weight.text
    let newWeight = weight1?.replacingOccurrences(of: ",", with: ".")
    var finalWeight = Double(newWeight ?? "") ?? 0
    
    if european == false {
        finalHeight = finalHeight / 0.0328084
        finalWeight = finalWeight / 2.20462
    }
    
    if gender.selectedSegmentIndex == 0 {
        BMMan = 66.4730 + (13.7156 * finalWeight) + (5.0033 * finalHeight) - (6.755 * age1)
    } else if gender.selectedSegmentIndex == 1 {
        BMWoman = 655.095 + (9.5634 * finalWeight) + (1.849 * finalHeight) - (4.6756 * age1)
    }
    }

    func calculateKcal() {
        let age1 = Double(age.text ?? "") ?? 0
        
        if gender.selectedSegmentIndex == 0 && workout.selectedSegmentIndex == 0 && dailyActivity.selectedSegmentIndex == 0 {
            switch age1 {
            case 0 ... 59:
                KcalMan = ((BMMan ?? 0) * 1.55)
            case 60 ... 74:
                KcalMan = ((BMMan ?? 0) * 1.51)
            default:
                KcalMan = ((BMMan ?? 0) * 1.51)
            }
        } else if gender.selectedSegmentIndex == 0 && workout.selectedSegmentIndex == 0 && dailyActivity.selectedSegmentIndex == 1 {
            switch age1 {
            case 0 ... 59:
                KcalMan = ((BMMan ?? 0) * 1.78)
            case 60 ... 74:
                KcalMan = ((BMMan ?? 0) * 1.51)
            default:
                KcalMan = ((BMMan ?? 0) * 1.51)
            }
        } else if gender.selectedSegmentIndex == 0 && workout.selectedSegmentIndex == 0 && dailyActivity.selectedSegmentIndex == 2 {
            switch age1 {
            case 0 ... 59:
                KcalMan = ((BMMan ?? 0) * 2.10)
            case 60 ... 74:
                KcalMan = ((BMMan ?? 0) * 1.51)
            default:
                KcalMan = ((BMMan ?? 0) * 1.51)
            }
        } else if gender.selectedSegmentIndex == 0 && workout.selectedSegmentIndex == 1 && dailyActivity.selectedSegmentIndex == 0 {
            switch age1 {
            case 0 ... 59:
                KcalMan = ((BMMan ?? 0) * 1.41)
            case 60 ... 74:
                KcalMan = ((BMMan ?? 0) * 1.40)
            default:
                KcalMan = ((BMMan ?? 0) * 1.33)
            }
        } else if gender.selectedSegmentIndex == 0 && workout.selectedSegmentIndex == 1 && dailyActivity.selectedSegmentIndex == 1 {
            switch age1 {
            case 0 ... 59:
                KcalMan = ((BMMan ?? 0) * 1.70)
            case 60 ... 74:
                KcalMan = ((BMMan ?? 0) * 1.40)
            default:
                KcalMan = ((BMMan ?? 0) * 1.33)
            }
        } else if gender.selectedSegmentIndex == 0 && workout.selectedSegmentIndex == 1 && dailyActivity.selectedSegmentIndex == 2 {
            switch age1 {
            case 0 ... 59:
                KcalMan = ((BMMan ?? 0) * 2.01)
            case 60 ... 74:
                KcalMan = ((BMMan ?? 0) * 1.40)
            default:
                KcalMan = ((BMMan ?? 0) * 1.33)
            }
        } else if gender.selectedSegmentIndex == 1 && workout.selectedSegmentIndex == 0 && dailyActivity.selectedSegmentIndex == 0 {
            switch age1 {
            case 0 ... 59:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            case 60 ... 74:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            default:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            }
        } else if gender.selectedSegmentIndex == 1 && workout.selectedSegmentIndex == 0 && dailyActivity.selectedSegmentIndex == 1 {
            switch age1 {
            case 0 ... 59:
                KcalWoman = ((BMWoman ?? 0) * 1.64)
            case 60 ... 74:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            default:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            }
        } else if gender.selectedSegmentIndex == 1 && workout.selectedSegmentIndex == 0 && dailyActivity.selectedSegmentIndex == 2 {
            switch age1 {
            case 0 ... 59:
                KcalWoman = ((BMWoman ?? 0) * 1.82)
            case 60 ... 74:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            default:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            }
        } else if gender.selectedSegmentIndex == 1 && workout.selectedSegmentIndex == 1 && dailyActivity.selectedSegmentIndex == 0 {
            switch age1 {
            case 0 ... 59:
                KcalWoman = ((BMWoman ?? 0) * 1.42)
            case 60 ... 74:
                KcalWoman = ((BMWoman ?? 0) * 1.44)
            default:
                KcalWoman = ((BMWoman ?? 0) * 1.37)
            }
        } else if gender.selectedSegmentIndex == 1 && workout.selectedSegmentIndex == 1 && dailyActivity.selectedSegmentIndex == 1 {
            switch age1 {
            case 0 ... 59:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            case 60 ... 74:
                KcalWoman = ((BMWoman ?? 0) * 1.44)
            default:
                KcalWoman = ((BMWoman ?? 0) * 1.37)
            }
        } else if gender.selectedSegmentIndex == 1 && workout.selectedSegmentIndex == 1 && dailyActivity.selectedSegmentIndex == 2 {
            switch age1 {
            case 0 ... 59:
                KcalWoman = ((BMWoman ?? 0) * 1.73)
            case 60 ... 74:
                KcalWoman = ((BMWoman ?? 0) * 1.44)
            default:
                KcalWoman = ((BMWoman ?? 0) * 1.37)
            }
        }
    }
    @IBAction func calculate(_ sender: Any) {
        
        if age.text == "" || weight.text == "" || height.text == "" {
            let alert = UIAlertController(title: "Warning", message: "Please, compile each form of the view!", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                if self.age.text == "" {
                    self.age.becomeFirstResponder()
                } else if self.height.text == "" {
                    self.height.becomeFirstResponder()
                } else if self.weight.text == "" {
                    self.weight.becomeFirstResponder()
                }
                
            }))
            
            let generator = UIImpactFeedbackGenerator(style: .light)
            generator.impactOccurred()
            self.present(alert, animated: true)
            
        } else {
            calculateBM()
            calculateKcal()
            if gender.selectedSegmentIndex == 0{
                Kcal = KcalMan
                labelStr = String(format: "%.0f", KcalMan!)
            } else if gender.selectedSegmentIndex == 1 {
                Kcal = KcalWoman
                labelStr = String(format: "%.0f", KcalWoman!)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ReviewCalorieController
        {
            let vc = segue.destination as! ReviewCalorieController
            vc.Kcal = Kcal ?? 0.0
        }
    }
    
}

    

extension CalorieController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
 func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
     // get the current text, or use an empty string if that failed
     let currentText = textField.text ?? ""

     // attempt to read the range they are trying to change, or exit if we can't
     guard let stringRange = Range(range, in: currentText) else { return false }

     // add their new text to the existing text
    let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

     // make sure the result is under 16 characters
    return updatedText.count <= 5
    }

}

extension UISegmentedControl{
    func selectedSegmentTintColor(_ color: UIColor) {
        self.setTitleTextAttributes([.foregroundColor: color], for: .selected)
    }
    func unselectedSegmentTintColor(_ color: UIColor) {
        self.setTitleTextAttributes([.foregroundColor: color], for: .normal)
    }
}

