//
//  ReviewNutrientsController.swift
//  PersonalTrainerApp
//
//  Created by Giovanni Carfora on 04/03/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit

class ReviewNutrientsController: UIViewController {
    @IBOutlet weak var carbView: UIView!
    @IBOutlet weak var proteinsView: UIView!
    @IBOutlet weak var fatsView: UIView!
    var carbohydratesGr: Double?
    var proteinsGr : Double?
    var fatsGr : Double?
    @IBOutlet weak var proteins: UILabel!
    @IBOutlet weak var carbohydrates: UILabel!
    @IBOutlet weak var fats: UILabel!
    @IBOutlet weak var done: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        done.layer.cornerRadius = 20
        carbView.layer.borderWidth = 2
        carbView.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        carbView.layer.cornerRadius = 10
        proteinsView.layer.borderWidth = 2
        proteinsView.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        proteinsView.layer.cornerRadius = 10
        fatsView.layer.borderWidth = 2
        fatsView.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        fatsView.layer.cornerRadius = 10
        let fatsFinal = String(format: "%.2f", fatsGr ?? 0)
        let proteinsFinal = String(format: "%.2f", proteinsGr ?? 0)
        let carbohydratesFinal = String(format: "%.2f", carbohydratesGr ?? 0)
        carbohydrates.text = "\(carbohydratesFinal) gr/day"
        proteins.text = "\(proteinsFinal) gr/day"
        fats.text = "\(fatsFinal) gr/day"
        
    }
    @IBAction func copy1(_ sender: Any) {
        
        UIPasteboard.general.string = carbohydrates.text
        
        let alert = UIAlertController(title: nil, message: "Succesfully copied the value in the clipboard!", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    @IBAction func copy2(_ sender: Any) {
        UIPasteboard.general.string = proteins.text
        
        let alert = UIAlertController(title: nil, message: "Succesfully copied the value in the clipboard!", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    @IBAction func copy3(_ sender: Any) {
        UIPasteboard.general.string = fats.text
        
        let alert = UIAlertController(title: nil, message: "Succesfully copied the value in the clipboard!", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    @IBAction func doneAct(_ sender: Any) {
        navigationController?.popViewControllers(viewsToPop: 2)
    }
}
