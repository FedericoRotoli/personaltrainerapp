//
//  ClientProfileController.swift
//  PersonalTrainerApp
//
//  Created by Federico Rotoli on 20/02/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ClientProfileController: UIViewController {
    
    var db: [NSManagedObject] = []
    
    var name = ""
    var surname = ""
    var age = 0
    var startWeight = 0
    var gender = ""
    var image = UIImage()
    var id = 0
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var dietPlanButton: UIButton!
    @IBOutlet weak var measurementsButton: UIButton!
    @IBOutlet weak var workoutPlanButton: UIButton!
    @IBOutlet weak var ageField: UITextField!
    @IBOutlet weak var genderField: UITextField!
    @IBOutlet weak var stWeightField: UITextField!
    @IBOutlet weak var acWeightField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        
        setup()
        
        self.title = "\(name) \(surname)"
    }
    
    func setup() {
        stWeightField.delegate = self
        ageField.delegate = self
        genderField.delegate = self
        acWeightField.delegate = self
        
        stWeightField.addDoneCancelToolbar()
        acWeightField.addDoneCancelToolbar()
        
        fetch()
        
         
        containerView.layer.borderWidth = 2
        containerView.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        containerView.layer.cornerRadius = 20
        
        measurementsButton.layer.cornerRadius = 10
        workoutPlanButton.layer.cornerRadius = 10
        dietPlanButton.layer.cornerRadius = 10
        
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = profileImage.bounds.width / 2
        profileImage.image = image
        ageField.text = "\(age)"
        genderField.text = "\(gender)"
        stWeightField.text = "\(startWeight)"
        if db[id].value(forKey: "actualweight") as! Int != 0 {
            acWeightField.text = "\(db[id].value(forKey: "actualweight") as! Int)"
        }
    }
    @IBAction func changeValue(_ sender: Any) {
        save()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.tabBarController?.tabBar.isHidden = true
     }
    
    func save() {
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Clienti", in: managedContext)
        let request = NSFetchRequest<NSFetchRequestResult>()
        request.entity = entity
        do {
            let results = try managedContext.fetch(request)
            let clientUpdate = results[id] as! NSManagedObject
            clientUpdate.setValue((Double(ageField.text ?? "") ?? 0), forKey: "age")
            clientUpdate.setValue((genderField.text ?? ""), forKey: "gender")
            clientUpdate.setValue((Double(stWeightField.text ?? "") ?? 0), forKey: "weight")
            clientUpdate.setValue((Double(acWeightField.text ?? "") ?? 0), forKey: "actualweight")
            do {
                try managedContext.save()
            }catch let error as NSError {
                print(error)
            }
        }
        catch let error as NSError {
            print(error)
        }
    }
    
    @IBAction func removeClient(_ sender: Any) {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler:
        {
            (alert: UIAlertAction!) -> Void in
            self.remove()
        })


        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func remove() {
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

        let client = db[id]

        managedContext.delete(client)

        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Error While Deleting Note: \(error.userInfo)")
        }
        
        NotificationCenter.default.post(name: Notification.Name.Action.reload2, object: nil)
        navigationController?.popViewController(animated: true)
    }
    
    func fetch(){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Clienti")
        //3
        do {
            db = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}

extension ClientProfileController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

           
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
