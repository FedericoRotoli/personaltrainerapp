//
//  NutrientsController.swift
//  PersonalTrainerApp
//
//  Created by Giovanni Carfora on 17/02/2020.
//  Copyright © 2020 Giovanni Carfora. All rights reserved.
//

import Foundation
import UIKit

class NutrientsController: UIViewController {
    
    private var BMMan: Double?
    private var BMWoman: Double?
    private var KcalMan: Double?
    private var KcalWoman: Double?
    private var carbohydratesGr: Double?
    private var proteinsGr: Double?
    private var fatsGr: Double?
    private var european: Bool = true
    
    @IBOutlet weak var calc: UIButton!
    @IBOutlet weak var amLabel: UILabel!
    @IBOutlet weak var euLabel: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var europeanButton: UIButton!
    @IBOutlet weak var americanButton: UIButton!
    @IBOutlet weak var genderSegmentedControl: UISegmentedControl!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var heightTextField: UITextField!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var goalsSegmentedControl: UISegmentedControl!
    @IBOutlet weak var dailyActivitySegmentedControl: UISegmentedControl!
    @IBOutlet weak var workOutSegmentedControl: UISegmentedControl!
    
    var doubleStr: Double?
    var proteinStr: Double?
    var carboStr: Double?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()

//    MARK: Hide text for carbohydrates, proteins and fats
        
                        
        ageTextField.addDoneCancelToolbar()
        weightTextField.addDoneCancelToolbar()
        heightTextField.addDoneCancelToolbar()
        ageTextField.delegate = self
        weightTextField.delegate = self
        heightTextField.delegate = self
        calc.layer.cornerRadius = 20


        viewContainer.layer.borderWidth = 2
        viewContainer.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        viewContainer.layer.cornerRadius = 10
        americanButton.layer.borderWidth = 2
        americanButton.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        americanButton.layer.cornerRadius = 20
        europeanButton.layer.borderWidth = 2
        europeanButton.layer.borderColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1).cgColor
        europeanButton.backgroundColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)
        euLabel.textColor = UIColor.white

        europeanButton.layer.cornerRadius = 20

        genderSegmentedControl.selectedSegmentTintColor(.white)
        dailyActivitySegmentedControl.selectedSegmentTintColor(.white)
        workOutSegmentedControl.selectedSegmentTintColor(.white)
        goalsSegmentedControl.selectedSegmentTintColor(.white)
        
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
        self.tabBarController?.tabBar.isHidden = true
        }
    
    override func viewWillAppear(_ animated: Bool) {
         self.tabBarController?.tabBar.isHidden = true
     }
    

    @IBAction func euSystemAction(_ sender: Any) {
//        if !european {
//            let generator = UIImpactFeedbackGenerator(style: .medium)
//             generator.impactOccurred()
//        }

        
        europeanButton.layer.borderColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1).cgColor
        europeanButton.backgroundColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)
        americanButton.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.2).cgColor
        americanButton.backgroundColor = .clear
        amLabel.textColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)
        euLabel.textColor = UIColor.white
        weightLabel.text = "Weight (in kg)"
        heightLabel.text = "Height (in cm)"
        weightTextField.text = ""
        heightTextField.text = ""
        weightTextField.placeholder = "Insert here"
        heightTextField.placeholder = "Insert here"
        european = true
    }
    @IBAction func americaSystemAction(_ sender: Any) {
//        if european {
//            let generator = UIImpactFeedbackGenerator(style: .medium)
//             generator.impactOccurred()
//        }
        americanButton.layer.borderColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1).cgColor
        americanButton.backgroundColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)
        europeanButton.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.2).cgColor
        europeanButton.backgroundColor = .clear

        euLabel.textColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)

        amLabel.textColor = UIColor.white
        weightLabel.text = "Weight (in lbs)"
        heightLabel.text = "Height (in ft)"
        weightTextField.text = ""
        heightTextField.text = ""
        weightTextField.placeholder = "Insert here"
        heightTextField.placeholder = "Insert here"
        european = false
    }
    
    func calculateBM() {
        
        let age = Double(ageTextField.text ?? "") ?? 0
        let height = heightTextField.text
        let newHeight = height?.replacingOccurrences(of: ",", with: ".")
        var finalHeight = Double(newHeight ?? "") ?? 0
        
        let weight = weightTextField.text
        let newWeight = weight?.replacingOccurrences(of: ",", with: ".")
        var finalWeight = Double(newWeight ?? "") ?? 0
        
        if european == false {
            finalHeight = finalHeight / 0.0328084
            finalWeight = finalWeight / 2.20462
        }
        
        if genderSegmentedControl.selectedSegmentIndex == 0 {
            BMMan = 66.4730 + (13.7156 * finalWeight) + (5.0033 * finalHeight) - (6.755 * age)
        } else if genderSegmentedControl.selectedSegmentIndex == 1 {
            BMWoman = 655.095 + (9.5634 * finalWeight) + (1.849 * finalHeight) - (4.6756 * age)
        }
    }
    
    func calculateKcal() {
        let age = Double(ageTextField.text ?? "") ?? 0
        
        if genderSegmentedControl.selectedSegmentIndex == 0 && workOutSegmentedControl.selectedSegmentIndex == 0 && dailyActivitySegmentedControl.selectedSegmentIndex == 0 {
            switch age {
            case 0 ... 59:
                KcalMan = ((BMMan ?? 0) * 1.55)
            case 60 ... 74:
                KcalMan = ((BMMan ?? 0) * 1.51)
            default:
                KcalMan = ((BMMan ?? 0) * 1.51)
            }
        } else if genderSegmentedControl.selectedSegmentIndex == 0 && workOutSegmentedControl.selectedSegmentIndex == 0 && dailyActivitySegmentedControl.selectedSegmentIndex == 1 {
            switch age {
            case 0 ... 59:
                KcalMan = ((BMMan ?? 0) * 1.78)
            case 60 ... 74:
                KcalMan = ((BMMan ?? 0) * 1.51)
            default:
                KcalMan = ((BMMan ?? 0) * 1.51)
            }
        } else if genderSegmentedControl.selectedSegmentIndex == 0 && workOutSegmentedControl.selectedSegmentIndex == 0 && dailyActivitySegmentedControl.selectedSegmentIndex == 2 {
            switch age {
            case 0 ... 59:
                KcalMan = ((BMMan ?? 0) * 2.10)
            case 60 ... 74:
                KcalMan = ((BMMan ?? 0) * 1.51)
            default:
                KcalMan = ((BMMan ?? 0) * 1.51)
            }
        } else if genderSegmentedControl.selectedSegmentIndex == 0 && workOutSegmentedControl.selectedSegmentIndex == 1 && dailyActivitySegmentedControl.selectedSegmentIndex == 0 {
            switch age {
            case 0 ... 59:
                KcalMan = ((BMMan ?? 0) * 1.41)
            case 60 ... 74:
                KcalMan = ((BMMan ?? 0) * 1.40)
            default:
                KcalMan = ((BMMan ?? 0) * 1.33)
            }
        } else if genderSegmentedControl.selectedSegmentIndex == 0 && workOutSegmentedControl.selectedSegmentIndex == 1 && dailyActivitySegmentedControl.selectedSegmentIndex == 1 {
            switch age {
            case 0 ... 59:
                KcalMan = ((BMMan ?? 0) * 1.70)
            case 60 ... 74:
                KcalMan = ((BMMan ?? 0) * 1.40)
            default:
                KcalMan = ((BMMan ?? 0) * 1.33)
            }
        } else if genderSegmentedControl.selectedSegmentIndex == 0 && workOutSegmentedControl.selectedSegmentIndex == 1 && dailyActivitySegmentedControl.selectedSegmentIndex == 2 {
            switch age {
            case 0 ... 59:
                KcalMan = ((BMMan ?? 0) * 2.01)
            case 60 ... 74:
                KcalMan = ((BMMan ?? 0) * 1.40)
            default:
                KcalMan = ((BMMan ?? 0) * 1.33)
            }
        } else if genderSegmentedControl.selectedSegmentIndex == 1 && workOutSegmentedControl.selectedSegmentIndex == 0 && dailyActivitySegmentedControl.selectedSegmentIndex == 0 {
            switch age {
            case 0 ... 59:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            case 60 ... 74:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            default:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            }
        } else if genderSegmentedControl.selectedSegmentIndex == 1 && workOutSegmentedControl.selectedSegmentIndex == 0 && dailyActivitySegmentedControl.selectedSegmentIndex == 1 {
            switch age {
            case 0 ... 59:
                KcalWoman = ((BMWoman ?? 0) * 1.64)
            case 60 ... 74:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            default:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            }
        } else if genderSegmentedControl.selectedSegmentIndex == 1 && workOutSegmentedControl.selectedSegmentIndex == 0 && dailyActivitySegmentedControl.selectedSegmentIndex == 2 {
            switch age {
            case 0 ... 59:
                KcalWoman = ((BMWoman ?? 0) * 1.82)
            case 60 ... 74:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            default:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            }
        } else if genderSegmentedControl.selectedSegmentIndex == 1 && workOutSegmentedControl.selectedSegmentIndex == 1 && dailyActivitySegmentedControl.selectedSegmentIndex == 0 {
            switch age {
            case 0 ... 59:
                KcalWoman = ((BMWoman ?? 0) * 1.42)
            case 60 ... 74:
                KcalWoman = ((BMWoman ?? 0) * 1.44)
            default:
                KcalWoman = ((BMWoman ?? 0) * 1.37)
            }
        } else if genderSegmentedControl.selectedSegmentIndex == 1 && workOutSegmentedControl.selectedSegmentIndex == 1 && dailyActivitySegmentedControl.selectedSegmentIndex == 1 {
            switch age {
            case 0 ... 59:
                KcalWoman = ((BMWoman ?? 0) * 1.56)
            case 60 ... 74:
                KcalWoman = ((BMWoman ?? 0) * 1.44)
            default:
                KcalWoman = ((BMWoman ?? 0) * 1.37)
            }
        } else if genderSegmentedControl.selectedSegmentIndex == 1 && workOutSegmentedControl.selectedSegmentIndex == 1 && dailyActivitySegmentedControl.selectedSegmentIndex == 2 {
            switch age {
            case 0 ... 59:
                KcalWoman = ((BMWoman ?? 0) * 1.73)
            case 60 ... 74:
                KcalWoman = ((BMWoman ?? 0) * 1.44)
            default:
                KcalWoman = ((BMWoman ?? 0) * 1.37)
            }
        }
    }
    
    
    func calculateNutrient() {
        calculateBM()
        calculateKcal()
        if goalsSegmentedControl.selectedSegmentIndex == 0 && genderSegmentedControl.selectedSegmentIndex == 0 {
            carbohydratesGr = ((KcalMan ?? 0) * 0.122)
//            carboStr = String(format: "%.2f", carbohydratesGr!)
//            carbohydrates.text = "\(carboStr) gr/day"
            proteinsGr = ((KcalMan ?? 0) * 0.073)
//            proteinStr = String(format: "%.2f", proteinsGr!)
//            proteins.text = "\(proteinStr) gr/day"
            fatsGr = ((KcalMan ?? 0) * 0.022)
//            doubleStr = String(format: "%.2f", fatsGr!)
//            fats.text = "\(doubleStr) gr/day"
        } else if goalsSegmentedControl.selectedSegmentIndex == 1 && genderSegmentedControl.selectedSegmentIndex == 0 {
            carbohydratesGr = ((KcalMan ?? 0) * 0.098)
//            carboStr = String(format: "%.2f", carbohydratesGr!)
//            carbohydrates.text = "\(carboStr) gr/day"
            proteinsGr = ((KcalMan ?? 0) * 0.073)
//            proteinStr = String(format: "%.2f", proteinsGr!)
//            proteins.text = "\(proteinStr) gr/day"
            fatsGr = ((KcalMan ?? 0) * 0.017)
//            doubleStr = String(format: "%.2f", fatsGr!)
//            fats.text = "\(doubleStr) gr/day"
        } else if goalsSegmentedControl.selectedSegmentIndex == 2 && genderSegmentedControl.selectedSegmentIndex == 0 {
            carbohydratesGr = ((KcalMan ?? 0) * 0.102)
//            carboStr = String(format: "%.2f", carbohydratesGr!)
//            carbohydrates.text = "\(carboStr) gr/day"
            proteinsGr = ((KcalMan ?? 0) * 0.049)
//            proteinStr = String(format: "%.2f", proteinsGr!)
//            proteins.text = "\(proteinStr) gr/day"
            fatsGr = ((KcalMan ?? 0) * 0.018)
//            doubleStr = String(format: "%.2f", fatsGr!)
//            fats.text = "\(doubleStr) gr/day"
        } else if goalsSegmentedControl.selectedSegmentIndex == 2 && genderSegmentedControl.selectedSegmentIndex == 1 {
            carbohydratesGr = ((KcalWoman ?? 0) * 0.102)
//            carboStr = String(format: "%.2f", carbohydratesGr!)
//            carbohydrates.text = "\(carboStr) gr/day"
            proteinsGr = ((KcalWoman ?? 0) * 0.049)
//            proteinStr = String(format: "%.2f", proteinsGr!)
//            proteins.text = "\(proteinStr) gr/day"
            fatsGr = ((KcalWoman ?? 0) * 0.018)
//            doubleStr = String(format: "%.2f", fatsGr!)
//            fats.text = "\(doubleStr) gr/day"
        } else if goalsSegmentedControl.selectedSegmentIndex == 0 && genderSegmentedControl.selectedSegmentIndex == 1 {
            carbohydratesGr = ((KcalWoman ?? 0) * 0.122)
//            carboStr = String(format: "%.2f", carbohydratesGr!)
//            carbohydrates.text = "\(carboStr) gr/day"
            proteinsGr = ((KcalWoman ?? 0) * 0.073)
//            proteinStr = String(format: "%.2f", proteinsGr!)
//            proteins.text = "\(proteinStr) gr/day"
            fatsGr = ((KcalWoman ?? 0) * 0.022)
//            doubleStr = String(format: "%.2f", fatsGr!)
//            fats.text = "\(doubleStr) gr/day"
        } else if goalsSegmentedControl.selectedSegmentIndex == 1 && genderSegmentedControl.selectedSegmentIndex == 1 {
            carbohydratesGr = ((KcalWoman ?? 0) * 0.098)
//            carboStr = String(format: "%.2f", carbohydratesGr!)
//            carbohydrates.text = "\(carboStr) gr/day"
            proteinsGr = ((KcalWoman ?? 0) * 0.073)
//            proteinStr = String(format: "%.2f", proteinsGr!)
//            proteins.text = "\(proteinStr) gr/day"
            fatsGr = ((KcalWoman ?? 0) * 0.017)
//            doubleStr = String(format: "%.2f", fatsGr!)
//            fats.text = "\(doubleStr) gr/day"
        }
    }
    
    @IBAction func calculator(_ sender: Any) {
        
        if ageTextField.text == "" || weightTextField.text == "" || heightTextField.text == "" {
            let alert = UIAlertController(title: "Warning", message: "Please, compile each form of the view!", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                if self.ageTextField.text == "" {
                    self.ageTextField.becomeFirstResponder()
                } else if self.heightTextField.text == "" {
                    self.heightTextField.becomeFirstResponder()
                } else if self.weightTextField.text == "" {
                    self.weightTextField.becomeFirstResponder()
                }
                
            }))
            
            let generator = UIImpactFeedbackGenerator(style: .light)
            generator.impactOccurred()
            self.present(alert, animated: true)
            
        } else {
            calculateNutrient()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ReviewNutrientsController
        {
            let vc = segue.destination as! ReviewNutrientsController
            vc.fatsGr = fatsGr ?? 0.0
            let cv = segue.destination as! ReviewNutrientsController
            cv.proteinsGr = proteinsGr ?? 0.0
            let cvc = segue.destination as! ReviewNutrientsController
            cvc.carbohydratesGr = carbohydratesGr ?? 0
        }
    }
}
extension NutrientsController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
 func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
     // get the current text, or use an empty string if that failed
     let currentText = textField.text ?? ""

     // attempt to read the range they are trying to change, or exit if we can't
     guard let stringRange = Range(range, in: currentText) else { return false }

     // add their new text to the existing text
    let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

     // make sure the result is under 16 characters
    return updatedText.count <= 5
    }

}

