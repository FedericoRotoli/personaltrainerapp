//
//  ViewController.swift
//  PersonalTrainerApp
//
//  Created by Federico Rotoli on 17/02/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import UIKit
import CoreData

extension UIDatePicker {
    
    func set18YearValidation() {
        let currentDate: Date = Date()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(identifier: "UTC")!
        var components: DateComponents = DateComponents()
        components.calendar = calendar
        components.year = 1
        let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!
        components.year = -1
        let minDate: Date = calendar.date(byAdding: components, to: currentDate)!
        self.minimumDate = minDate
        self.maximumDate = maxDate
    }
    
}

extension Notification.Name {
    struct Action {
        static let reload = Notification.Name("reload")
        static let reload2 = Notification.Name("reload2")
    }
}

class HomeController: UIViewController, UITextFieldDelegate {
    
    let datePicker = UIDatePicker()
    
    var pickerInputField: UITextField!
    
    var compareDay = ""
    var timer: Timer?
    var modify = false
    var db: [NSManagedObject] = []
    var reminder: NSManagedObject!
    var id = 0
    let colors = [UIColor(red: 253/256, green: 241/256, blue: 219/256, alpha: 1), UIColor(red: 218/256, green: 246/256, blue: 244/256, alpha: 1), UIColor(red: 207/256, green: 236/256, blue: 255/256, alpha: 1)]
    var element: NSManagedObject!
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var myTable: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var checkLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerInputField = {
             let field = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
               self.view.addSubview(field)
               field.delegate = self
             return field
           }()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: Notification.Name.Action.reload, object: nil)
        showDatePicker()
        fetch()
        
        let date = Date()
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "dd MMM"
        compareDay = formatter2.string(from: date)
        
        setupTableView()
        viewWillLayoutSubviews()
        
        if db.count == 0 {
            checkLabel.isHidden = false
        } else {
            checkLabel.isHidden = true
        }
        
        getTimeOfDate()
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.getTimeOfDate), userInfo: nil, repeats: true)
        navigationController?.navigationBar.prefersLargeTitles = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetch()
        myTable.reloadData()
        if db.count == 0 {
            checkLabel.isHidden = false
        } else {
            checkLabel.isHidden = true
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        myTable.separatorInset = UIEdgeInsets.zero
        myTable.layoutMargins = UIEdgeInsets.zero
    }
    
    @objc func getTimeOfDate() {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM - HH:mm"
        let result = formatter.string(from: date)
        
        dateLabel.text = result
        dateLabel.sizeToFit()
    }
    
    @objc func reload() {
        fetch()
        myTable.reloadData()
        if db.count == 0 {
            checkLabel.isHidden = false
        } else {
            checkLabel.isHidden = true
        }
    }
    
    private func setupTableView(){
        let bundle = Bundle(for: type(of: self))
        
        myTable.delegate = self
        myTable.dataSource = self
        let cellNib = UINib(nibName: "TableViewCell", bundle: bundle)
        myTable.register(cellNib, forCellReuseIdentifier: "cellIdentifier")
        
        self.myTable.rowHeight = 90.0
        
    }


    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        datePicker.set18YearValidation()

       //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)

        pickerInputField.inputAccessoryView = toolbar
        pickerInputField.inputView = datePicker
        
    }
    
    @IBAction func calendarTap(_ sender: Any) {
          if pickerInputField.isFirstResponder {
            pickerInputField.resignFirstResponder()
          }

          pickerInputField.becomeFirstResponder()
    }
    
      @objc func donedatePicker(){

        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM"
        dayLabel.text = formatter.string(from: datePicker.date)
        compareDay = formatter.string(from: datePicker.date)
        fetch()
        reload()
        self.view.endEditing(true)
     }

     @objc func cancelDatePicker(){
        self.view.endEditing(true)
      }

}

extension HomeController: UITableViewDelegate, UITableViewDataSource {
    
    private func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {

        cell.layoutMargins = UIEdgeInsets.zero
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return db.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath) as! TableViewCell
        
        let reminder = db[indexPath.row]
        let tag = reminder.value(forKey: "tag") as? String
        cell.titleLabel.text = reminder.value(forKey: "title") as? String
        cell.descLabel.text = reminder.value(forKey: "details") as? String
        cell.timerLabel.text = reminder.value(forKey: "time") as? String
        cell.personLabel.text = reminder.value(forKey: "people") as? String
        cell.tagLabel.text = tag
        
        switch (tag) {
            
        case "Training":
            cell.titleContainer.backgroundColor = colors[0]
            break
            
        case "Dining":
            cell.titleContainer.backgroundColor = colors[1]
            break
            
        case "Meeting":
            cell.titleContainer.backgroundColor = colors[2]
            break
            
        default:
            break
        }
        
        cell.titleLabel.sizeToFit()
        cell.titleContainer.layer.cornerRadius = 15
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

            let note = db[indexPath.row]

            managedContext.delete(note)

            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Error While Deleting Note: \(error.userInfo)")
            }
        }
        
        fetch()
        myTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        reminder = db[indexPath.row]
        id = indexPath.row
        modify = true
        performSegue(withIdentifier: "clientSegue", sender: self)
    }
    

    
    func fetch(){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Reminder")
        let predicate = NSPredicate(format: "day = %@", compareDay)
        let sort = NSSortDescriptor(key: "time", ascending: true)
        fetchRequest.sortDescriptors = [sort]
        fetchRequest.predicate = predicate
        //3
        do {
            db = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")

        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is PlusController
        {
            if modify {
                let vc = segue.destination as! PlusController
                vc.eventId = id
                vc.eventTitle = reminder.value(forKey: "title") as! String
                vc.eventDesc = reminder.value(forKey: "details") as! String
                vc.eventPeople = reminder.value(forKey: "people") as! String
                vc.selectedTag = reminder.value(forKey: "tag") as! String
                vc.modify = true
            } else {
                let vc = segue.destination as! PlusController
                vc.modify = false
            }
            
            modify = false
        }
    }
}



