//
//  BodymassController.swift
//  PersonalTrainerApp
//
//  Created by Federico Rotoli on 17/02/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {

  func popViewControllers(viewsToPop: Int, animated: Bool = true) {
    if viewControllers.count > viewsToPop {
      let vc = viewControllers[viewControllers.count - viewsToPop - 1]
      popToViewController(vc, animated: animated)
    }
  }

}

extension UITextField {
    func addDoneCancelToolbar(onDone: (target: Any, action: Selector)? = nil, onCancel: (target: Any, action: Selector)? = nil) {
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))

        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .done, target: onDone.target, action: onDone.action)
        ]
        toolbar.sizeToFit()

        self.inputAccessoryView = toolbar
    }

    // Default actions:
    @objc func doneButtonTapped() { self.resignFirstResponder() }
}


class BodymassController: UIViewController  {
    
    @IBOutlet weak var amLabel: UILabel!
    @IBOutlet weak var euLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var weightContainer: UIView!
    @IBOutlet weak var weightField: UITextField!
    @IBOutlet weak var heightField: UITextField!
    @IBOutlet weak var heightContainer: UIView!
    
    
    var bmiGlobal : Double  = 0
    var bmi = 0.0
    var european = true
    
    @IBOutlet weak var calculateBtn: UIButton!
    @IBOutlet weak var americanButton: UIButton!
    @IBOutlet weak var europeanButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        self.tabBarController?.tabBar.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        weightField.addDoneCancelToolbar()
        heightField.addDoneCancelToolbar()
        
        weightField.delegate = self
        heightField.delegate = self
        
        calculateBtn.layer.cornerRadius = 20
        weightContainer.layer.borderWidth = 2
        weightContainer.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        weightContainer.layer.cornerRadius = 20
        heightContainer.layer.borderWidth = 2
        heightContainer.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        heightContainer.layer.cornerRadius = 20
        
        americanButton.layer.borderWidth = 2
        americanButton.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        americanButton.layer.cornerRadius = 20
        europeanButton.layer.borderWidth = 2
        europeanButton.layer.borderColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1).cgColor
        europeanButton.backgroundColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)
        euLabel.textColor = UIColor.white
        
        europeanButton.layer.cornerRadius = 20
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

 @objc func keyboardWillShow(notification: NSNotification) {
     if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
         if self.view.frame.origin.y == 0 {
            self.view.frame.origin.y -= keyboardSize.height/1.5
         }
     }
 }

 @objc func keyboardWillHide(notification: NSNotification) {
     if self.view.frame.origin.y != 0 {
         self.view.frame.origin.y = 0
     }
 }

    @IBAction func bmiCalculate(_ sender: Any) {
        if weightField.text == "" || heightField.text == "" {
            
            let alert = UIAlertController(title: "Warning", message: "Please, compile each form of the view!", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                if self.weightField.text == "" {
                    self.weightField.becomeFirstResponder()
                } else if self.heightField.text == "" {
                    self.heightField.becomeFirstResponder()
                }
                
            }))
            
            let generator = UIImpactFeedbackGenerator(style: .light)
            generator.impactOccurred()
            self.present(alert, animated: true)
        }
        
        if european {
            calculateis()
        } else {
            calculateusc()
        }
        
        
    }
    @IBAction func euSystemAction(_ sender: Any) {
//        if !european {
//            let generator = UIImpactFeedbackGenerator(style: .medium)
//             generator.impactOccurred()
//        }

        
        europeanButton.layer.borderColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1).cgColor
        europeanButton.backgroundColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)
        americanButton.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.2).cgColor
        americanButton.backgroundColor = .clear
        amLabel.textColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)
        euLabel.textColor = UIColor.white
        weightLabel.text = "Weight (in kg)"
        heightLabel.text = "Height (in cm)"
        european = true
    }
    
    @IBAction func americaSystemAction(_ sender: Any) {
//        if european {
//            let generator = UIImpactFeedbackGenerator(style: .medium)
//             generator.impactOccurred()
//        }
        americanButton.layer.borderColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1).cgColor
        americanButton.backgroundColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)
        europeanButton.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.2).cgColor
        europeanButton.backgroundColor = .clear
        
        euLabel.textColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1)
        
        amLabel.textColor = UIColor.white
        weightLabel.text = "Weight (in lbs)"
        heightLabel.text = "Height (in ft)"
        european = false
    }
    
    func calculateis() {
        let weight = weightField.text
        let newWeight = weight?.replacingOccurrences(of: ",", with: ".")
        let finalWeight = Double(newWeight ?? "") ?? 0
                
        let height = heightField.text
        let newHeight = height?.replacingOccurrences(of: ",", with: ".")
        var finalHeight = Double(newHeight ?? "") ?? 0
            
        finalHeight = finalHeight / 100

        
        bmi = finalWeight / (finalHeight * finalHeight)
        print(bmi)
        
    }
    
    func calculateusc() {
          let weight = weightField.text
                 let newWeight = weight?.replacingOccurrences(of: ",", with: ".")
        var finalWeight = Double(newWeight ?? "") ?? 0

        let height = heightField.text
        let newHeight = height?.replacingOccurrences(of: ",", with: ".")
        var finalHeight = Double(newHeight ?? "") ?? 0
        
        finalHeight = finalHeight / 2.9
        finalWeight = finalWeight / 2.205
                 
        bmi = finalWeight / (finalHeight * finalHeight)
        print(bmi)
      }

}
    
    extension BodymassController: UITextFieldDelegate {
           func textFieldShouldReturn(_ textField: UITextField) -> Bool {
               self.view.endEditing(true)
               return false
           }

           
           func hideKeyboardWhenTappedAround() {
               let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
               tap.cancelsTouchesInView = false
               view.addGestureRecognizer(tap)
           }

           @objc func dismissKeyboard() {
               view.endEditing(true)
           }
       
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            // get the current text, or use an empty string if that failed
            let currentText = textField.text ?? ""

            // attempt to read the range they are trying to change, or exit if we can't
            guard let stringRange = Range(range, in: currentText) else { return false }

            // add their new text to the existing text
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

            // make sure the result is under 16 characters
            return updatedText.count <= 4
        }
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.destination is ReviewBmiController
            {
                let vc = segue.destination as! ReviewBmiController
                vc.bmi = bmi
            }
        }

}
    
    
    
    
   

    
//    func calculateBmi(){
////        calcolo del bmi
//
//        var height = Int(heightField.text ?? "") ?? 0
//        var weight = Int(weightField.text ?? "") ?? 0
//        print(height)
//        print(weight)
//        let doubleHeight = (height * height)
//        let bmi = weight / doubleHeight
//        print(bmi)
//
//    }
//
//
//    func textFieldDidEndEditing(_ textField: UITextField) {
//
//
//        if (textField == weightField){
////            print("edit for weight ended")
//            weightEdited = true
//
//
//
//        } else if textField == heightField{
////            print("edit for height ended")
//            heightEdited = true
//
//        }
//        if(weightEdited == true && heightEdited == true){
////            print("it was true")
//            calculateBmi()
//
////            print(bmi)
//
//
//
//
////            bmiLabel.isHidden = false
//
//
//        }else{
//        print ("no")
//        }
//
//    }
   












