//
//  PlusController.swift
//  PersonalTrainerApp
//
//  Created by Luciano Amoroso on 18/02/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class PlusController: UIViewController {
    
    var eventTitle = ""
    var eventDesc = ""
    var eventPeople = ""
    var modify = false
    var eventId = 0
    
    var db: [NSManagedObject] = []
    var selectedClient: NSManagedObject!
    
    var picker  = UIPickerView()
    var pickerInputField: UITextField!
    
    
    var strDay = ""
    var strDate = ""
    var selectedTag = "Training"

    let colors = [UIColor(red: 253/256, green: 241/256, blue: 219/256, alpha: 1), UIColor(red: 218/256, green: 246/256, blue: 244/256, alpha: 1), UIColor(red: 207/256, green: 236/256, blue: 255/256, alpha: 1)]
    
    @IBOutlet weak var addPeopleButton: UIButton!
    @IBOutlet weak var infoForm: UIView!
    @IBOutlet weak var peoplePicker: UITextField!
    @IBOutlet weak var titlePicker: UITextField!
    @IBOutlet weak var descriptionPicker: UITextField!
    @IBOutlet weak var startDate: UIDatePicker!
    @IBOutlet weak var tagControl: UISegmentedControl!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerInputField = {
             let field = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
               self.view.addSubview(field)
               field.delegate = self
             return field
           }()
        showPicker()
        
        fetch()
        
        if db.count == 0 {
            addPeopleButton.isHidden = true
        }
        
        setupDay()
        titlePicker.delegate = self
        descriptionPicker.delegate = self
        peoplePicker.addDoneCancelToolbar()
        titlePicker.addDoneCancelToolbar()
        descriptionPicker.addDoneCancelToolbar()
        hideKeyboardWhenTappedAround()
        tagControl.selectedSegmentTintColor(.white)
        infoForm.layer.borderWidth = 2
        infoForm.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        infoForm.layer.cornerRadius = 20
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if modify {
            titleLabel.text = "Edit Reminder"
            
            titlePicker.text = eventTitle
            descriptionPicker.text = eventDesc
            peoplePicker.text = eventPeople
            
            switch selectedTag {
            case "Training":
                tagControl.selectedSegmentIndex = 0
            case "Dining":
                tagControl.selectedSegmentIndex = 1
            case "Meeting":
                tagControl.selectedSegmentIndex = 2
            default:
                break
            }
        }
        
        else {
            titleLabel.text = "New Reminder"
            
            titlePicker.text = ""
            descriptionPicker.text = ""
            peoplePicker.text = ""
            tagControl.selectedSegmentIndex = 0
        }
    }
    
    func setupDay() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        strDate = dateFormatter.string(from: startDate.date)
        dateFormatter.dateFormat = "dd"
        strDay = dateFormatter.string(from: startDate.date)
    }
    
    @IBAction func plusTapped(_ sender: Any) {
        if selectedClient ==  nil {
            selectedClient = db[0]
        }
        
        if pickerInputField.isFirstResponder {
          pickerInputField.resignFirstResponder()
        }

        pickerInputField.becomeFirstResponder()
    }
    
    func showPicker(){
       //ToolBar
        let toolBar = UIToolbar();
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPicker));
        toolBar.items = [cancelButton, spaceButton, UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(donePicker))]
        
        picker.delegate = self
        picker.dataSource = self

        pickerInputField.inputAccessoryView = toolBar
        pickerInputField.inputView = picker
        
    }
    
      @objc func donePicker(){

        let name = "\(selectedClient.value(forKey: "name") ?? "") \(selectedClient.value(forKey: "surname") ?? "")"
        peoplePicker.text = name
        
        self.pickerInputField.endEditing(true)
     }

     @objc func cancelPicker(){
        self.pickerInputField.endEditing(true)
      }
    
    @IBAction func datePickerOk(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        strDate = dateFormatter.string(from: startDate.date)
        dateFormatter.dateFormat = "dd MMM"
        strDay = dateFormatter.string(from: startDate.date)
    }
  
    @IBAction func dismissTap(_ sender: Any) {
        dismiss(animated: true, completion:{})
    }
    
    @IBAction func doneButton(_ sender: Any) {
        
        if modify {
            if peoplePicker.text == "" || titlePicker.text == "" || descriptionPicker.text == "" {
                
                let alert = UIAlertController(title: "Warning", message: "Please, compile each form of the view!", preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                    if self.titlePicker.text == "" {
                        self.titlePicker.becomeFirstResponder()
                    } else if self.descriptionPicker.text == "" {
                        self.descriptionPicker.becomeFirstResponder()
                    } else if self.peoplePicker.text == "" {
                        self.peoplePicker.becomeFirstResponder()
                    }
                    
                }))
                
                let generator = UIImpactFeedbackGenerator(style: .light)
                generator.impactOccurred()

                self.present(alert, animated: true)
            }else {
                let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Reminder")
                let sort = NSSortDescriptor(key: "time", ascending: true)
                request.sortDescriptors = [sort]
                do {
                    let results = try managedContext.fetch(request)
                    let reminderUpdate = results[eventId] as! NSManagedObject
                    reminderUpdate.setValue(titlePicker.text!, forKey: "title")
                    reminderUpdate.setValue(descriptionPicker.text!, forKey: "details")
                    reminderUpdate.setValue(strDate, forKey: "time")
                    reminderUpdate.setValue(selectedTag, forKey: "tag")
                    reminderUpdate.setValue(peoplePicker.text!, forKey: "people")
                    reminderUpdate.setValue(strDay, forKey: "day")
                    do {
                        try managedContext.save()
                    }catch let error as NSError {
                        print(error)
                    }
                }
                catch let error as NSError {
                    print(error)
                }
                
                dismiss(animated: true, completion: {
                    NotificationCenter.default.post(name: Notification.Name.Action.reload, object: nil)
                })
                    
            }
            
            
        } else {
            if peoplePicker.text == "" || titlePicker.text == "" || descriptionPicker.text == "" {
                
                let alert = UIAlertController(title: "Warning", message: "Please, compile each form of the view!", preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                    if self.titlePicker.text == "" {
                        self.titlePicker.becomeFirstResponder()
                    } else if self.descriptionPicker.text == "" {
                        self.descriptionPicker.becomeFirstResponder()
                    } else if self.peoplePicker.text == "" {
                        self.peoplePicker.becomeFirstResponder()
                    }
                    
                }))
                
                let generator = UIImpactFeedbackGenerator(style: .light)
                generator.impactOccurred()

                self.present(alert, animated: true)
            } else {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let context = appDelegate.persistentContainer.viewContext
                let entity = NSEntityDescription.entity(forEntityName: "Reminder", in: context)
                      
                let item = NSManagedObject(entity: entity!, insertInto: context)
                item.setValue(titlePicker.text!, forKey: "title")
                item.setValue(descriptionPicker.text!, forKey: "details")
                item.setValue(strDate, forKey: "time")
                item.setValue(selectedTag, forKey: "tag")
                item.setValue(peoplePicker.text!, forKey: "people")
                item.setValue(strDay, forKey: "day")
                
                do {
                    try context.save()
                } catch {
                    print("Failed saving")
                }
                
                dismiss(animated: true, completion: {
                    NotificationCenter.default.post(name: Notification.Name.Action.reload, object: nil)
                })
            }
        }


    }
    
    func fetch(){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Clienti")
        //3
        do {
            db = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")

        }
        
    }
    
    @IBAction func indexChanged(_ sender: Any) {
        dismissKeyboard()
        
        switch tagControl.selectedSegmentIndex
        {
          case 0:
            selectedTag = "Training"
          case 1:
            selectedTag = "Dining"
          case 2:
            selectedTag = "Meeting"
          default:
            break
        }
    }
}

extension PlusController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension PlusController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return db.count
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let client = db[row]
        let name = "\(client.value(forKey: "name") ?? "") \(client.value(forKey: "surname") ?? "")"
        return name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selectedClient = db[row]
    }
}


