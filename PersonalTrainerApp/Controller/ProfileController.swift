//
//  ProfileController.swift
//  PersonalTrainerApp
//
//  Created by Federico Rotoli on 19/02/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit

extension UserDefaults {
    func imageForKey(key: String) -> UIImage? {
        var image: UIImage?
        if let imageData = data(forKey: key) {
            image = NSKeyedUnarchiver.unarchiveObject(with: imageData) as? UIImage
        }
        return image
    }
    func setImage(image: UIImage?, forKey key: String) {
        var imageData: NSData?
        if let image = image {
            imageData = NSKeyedArchiver.archivedData(withRootObject: image) as NSData?
        }
        set(imageData, forKey: key)
    }
}

class ProfileController: UIViewController {
    
    var imagePicker = UIImagePickerController()
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        nameField.delegate = self
        imagePicker.delegate = self
        
        hideKeyboardWhenTappedAround()
        tabBarController?.tabBar.isHidden = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = profileImage.bounds.width / 2
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func setup() {
        if let name = defaults.string(forKey: "name") {
            nameField.text = name
        }
        if let image = defaults.imageForKey(key: "profileImage") {
            profileImage.image = image
        }
    }
    
    @objc func imageTapped() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = true

            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if nameField.text == "" || profileImage.image == UIImage(systemName: "person.circle") {
            let alert = UIAlertController(title: "Warning", message: "Please, compile each form of the view!", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

            self.present(alert, animated: true)
        } else {
            defaults.set(nameField.text, forKey: "name")
            defaults.setImage(image: profileImage.image, forKey: "profileImage")
        }
        
        navigationController?.popViewController(animated: true)
        
    }
}


extension ProfileController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            profileImage.contentMode = .scaleToFill
            profileImage.image = pickedImage
        }
     
        dismiss(animated: true, completion: nil)
    }
}



extension ProfileController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
