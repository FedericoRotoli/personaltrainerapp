//
//  AddClientController.swift
//  PersonalTrainerApp
//
//  Created by Luciano Amoroso on 19/02/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class AddClientController: UIViewController {
    var gravity = "Male"
    var choose = false

    @IBOutlet weak var segGender: UISegmentedControl!

    @IBOutlet weak var formContainer: UIView!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var surname: UITextField!
    @IBOutlet weak var weight: UITextField!
    @IBOutlet weak var age: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    var imagePicker = UIImagePickerController()

override func viewDidLoad() {
super.viewDidLoad()
    
    name.delegate = self
    surname.delegate = self
    weight.delegate = self
    age.delegate = self
    
  imagePicker.delegate = self
         
         hideKeyboardWhenTappedAround()
         tabBarController?.tabBar.isHidden = true
         
         let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
         
        formContainer.layer.borderWidth = 2
        formContainer.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        formContainer.layer.cornerRadius = 20
        segGender.selectedSegmentTintColor(.white)
         profileImage.layer.masksToBounds = true
         profileImage.layer.cornerRadius = profileImage.bounds.width / 2
         profileImage.isUserInteractionEnabled = true
         profileImage.addGestureRecognizer(tapGestureRecognizer)
   
}


    @objc func imageTapped() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = true
            choose = true
            present(imagePicker, animated: true, completion: nil)
        }
    }
    @IBAction func dismissTap(_ sender: Any) {
        dismiss(animated: true, completion:{})
    }
    
    func hideKeyboardWhenTappedAround() {
          let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
          tap.cancelsTouchesInView = false
          view.addGestureRecognizer(tap)
      }

      @objc func dismissKeyboard() {
          view.endEditing(true)
      }
    
    @IBAction func doneButton(_ sender: Any) {
        
        if (name.text == "" || !choose) {
            let alert = UIAlertController(title: "Warning!", message: "Please, compile each form!", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                if self.name.text == "" {
                    self.name.becomeFirstResponder()
                } else if self.surname.text == "" {
                    self.surname.becomeFirstResponder()
                } else if self.age.text == "" {
                    self.age.becomeFirstResponder()
                } else if self.weight.text == "" {
                    self.weight.becomeFirstResponder()
                }
            }))
                   
            let generator = UIImpactFeedbackGenerator(style: .light)
            generator.impactOccurred()

            self.present(alert, animated: true)
            
        } else if (surname.text == "" || age.text == "" || weight.text == "" ){
            
            let alert = UIAlertController(title: "Warning!", message: "Please, compile each form!", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                if self.name.text == "" {
                    self.name.becomeFirstResponder()
                } else if self.surname.text == "" {
                    self.surname.becomeFirstResponder()
                } else if self.age.text == "" {
                    self.age.becomeFirstResponder()
                } else if self.weight.text == "" {
                    self.weight.becomeFirstResponder()
                }
            }))
                   
            let generator = UIImpactFeedbackGenerator(style: .light)
            generator.impactOccurred()

            self.present(alert, animated: true)
            
        } else {
        
           let appDelegate = UIApplication.shared.delegate as! AppDelegate
               let context = appDelegate.persistentContainer.viewContext
               let entity = NSEntityDescription.entity(forEntityName: "Clienti", in: context)
            
            if (segGender.selectedSegmentIndex == 0) {
                gravity = "Male"
            }else{
                gravity = "Female"
            }
                  
               let item = NSManagedObject(entity: entity!, insertInto: context)
                  item.setValue(name.text!, forKey: "name")
                  item.setValue(surname.text!, forKey: "surname")
               let eta = Int(age.text ?? "") ?? 0
                  item.setValue(eta, forKey: "age")
               let peso = Int(weight.text ?? "") ?? 0
                  item.setValue(peso, forKey: "weight")
                  item.setValue(gravity, forKey: "gender")
        
               let image = (profileImage.image)!.pngData()
                  item.setValue(image, forKey: "img")
                          
                  do {
                     try context.save()
                    } catch {
                     print("Failed saving")
                  }
                  dismiss(animated: true, completion: {
                        NotificationCenter.default.post(name: Notification.Name.Action.reload2, object: nil)
                  })
    }
    }


}

extension AddClientController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            profileImage.contentMode = .scaleToFill
            profileImage.image = pickedImage
        }
     
        dismiss(animated: true, completion: nil)
    }
}



extension AddClientController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

}

    



