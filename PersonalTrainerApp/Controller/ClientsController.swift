//
//  ClientsController.swift
//  PersonalTrainerApp
//
//  Created by Federico Rotoli on 17/02/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ClientsController: UIViewController {
    
    var id = 0
    var client: NSManagedObject!
    var db: [NSManagedObject] = []
    let colors = [UIColor(red: 253/256, green: 241/256, blue: 219/256, alpha: 1), UIColor(red: 218/256, green: 246/256, blue: 244/256, alpha: 1), UIColor(red: 207/256, green: 236/256, blue: 255/256, alpha: 1)]
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    @IBOutlet weak var myCollection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetch()
        viewWillLayoutSubviews()
        setupCollectionView()
        NotificationCenter.default.addObserver(self, selector: #selector(reload2), name: Notification.Name.Action.reload2, object: nil)
        
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupCollectionViewItemSize()
    }
    
    private func setupCollectionView(){
        myCollection.delegate = self
        myCollection.dataSource = self
        let nib = UINib(nibName: "ClientCollectionViewCell", bundle: nil)
        myCollection.register(nib, forCellWithReuseIdentifier: "cellIdentifier")
    }
    
    private func setupCollectionViewItemSize() {
        if collectionViewFlowLayout == nil {
            let lineSpacing: CGFloat = 20
            let interItemSpacing: CGFloat = 10
            
            let width = Int(view.frame.width - 30)
            let height = 80
            
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            
            collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            collectionViewFlowLayout.sectionInset = UIEdgeInsets.zero
            collectionViewFlowLayout.scrollDirection = .vertical
            collectionViewFlowLayout.minimumLineSpacing = lineSpacing
            collectionViewFlowLayout.minimumInteritemSpacing = interItemSpacing
            
            myCollection.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
        }
    }
    
    @objc func reload2() {
        fetch()
        myCollection.reloadData()
    }
}

extension ClientsController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return db.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdentifier", for: indexPath) as! ClientCollectionViewCell
        let client = db[indexPath.row]
        let image = UIImage(data: client.value(forKey: "img") as! Data)
        let name = client.value(forKey: "name") as? String
        let surname = client.value(forKey: "surname") as? String
        cell.nameLabel.text = "\(name ?? "") \(surname ?? "")"
        cell.containerView.layer.cornerRadius = 20

        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 4.0
        cell.layer.shadowOpacity = 0.20
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: 20).cgPath
        
        if self.traitCollection.userInterfaceStyle == .dark {
            cell.containerView.backgroundColor = .clear
            cell.nameLabel.textColor = .white
        } else {
            cell.containerView.backgroundColor = .white
            cell.nameLabel.textColor = .black
        }
        
        cell.containerView.layer.borderWidth = 1
        cell.containerView.layer.borderColor = UIColor.init(red: 69/256, green: 103/256, blue: 255/256, alpha: 1).cgColor
        cell.profileImage.layer.masksToBounds = true
        cell.profileImage.layer.cornerRadius = cell.profileImage.bounds.width / 2
        cell.profileImage.image = image
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        client = db[indexPath.row]
        id = indexPath.row
        performSegue(withIdentifier: "profileSegue", sender: self)
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        if let collection = myCollection {
            collection.reloadData()
        }
    }
    
    
    func fetch(){
         
         guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
             return
         }
         let managedContext = appDelegate.persistentContainer.viewContext
         //2
         let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Clienti")
         //3
         do {
             db = try managedContext.fetch(fetchRequest)
         } catch let error as NSError {
             print("Could not fetch. \(error), \(error.userInfo)")

         }
         
     }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is ClientProfileController
        {
            let vc = segue.destination as! ClientProfileController
            vc.name = client.value(forKey: "name") as! String
            vc.surname = client.value(forKey: "surname") as! String
            vc.age = client.value(forKey: "age") as! Int
            vc.gender = client.value(forKey: "gender") as! String
            vc.startWeight = client.value(forKey: "weight") as! Int
            vc.image = UIImage(data: client.value(forKey: "img") as! Data)!
            vc.id = id
        }
    }
}
