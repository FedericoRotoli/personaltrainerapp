//
//  ReviewBmiController.swift
//  PersonalTrainerApp
//
//  Created by Federico Rotoli on 03/03/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit

class ReviewBmiController: UIViewController {
    
    var bmi = 0.0
    @IBOutlet weak var bmiContainer: UIView!
    @IBOutlet weak var bmiLabel: UILabel!
    @IBOutlet weak var bmiStatement: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        doneButton.layer.cornerRadius = 20
        bmiContainer.layer.borderWidth = 2
        bmiContainer.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.3).cgColor
        bmiContainer.layer.cornerRadius = 10
        let bmiString = String(format: "%.1f", bmi)
        
        bmiLabel.text = bmiString
        checkBmi()
    }
    
        func checkBmi(){
            if (bmi < 15){
                bmiStatement.text = ("Very severely underweight")
                bmiLabel.textColor = UIColor.red
            }else if (bmi < 16){
                bmiStatement.text = ("Severely underweight    ")
                bmiLabel.textColor = UIColor.orange
            } else if(bmi <= 18.5){
                bmiStatement.text = ("Underweight")
                bmiLabel.textColor = UIColor.yellow
            } else if (bmi <= 25){
                bmiStatement.text = ("Normal (healthy weight)")
                bmiLabel.textColor = UIColor.green
            }else if (bmi <= 30){
                bmiStatement.text = ("Overweight")
                bmiLabel.textColor = UIColor.yellow
            }else if (bmi <= 35){
                bmiStatement.text = ("Moderately obese")
                bmiLabel.textColor = UIColor.orange
            }else if (bmi  <= 40){
                bmiStatement.text = ("Severely obese")
                bmiLabel.textColor = UIColor.red
            }else if (bmi > 40){
                bmiStatement.text = ("Very severely obese")
                bmiLabel.textColor = UIColor.red

            }


        }
    
    @IBAction func copyTapped(_ sender: Any) {
        UIPasteboard.general.string = bmiLabel.text
        
        let alert = UIAlertController(title: nil, message: "Succesfully copied the value in the clipboard!", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        navigationController?.popViewControllers(viewsToPop: 2)
    }
}
