//
//  CollectionViewCell.swift
//  PersonalTrainerApp
//
//  Created by Federico Rotoli on 19/02/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var checkmarkImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
