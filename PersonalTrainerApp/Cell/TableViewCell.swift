//
//  TableViewCell.swift
//  PersonalTrainerApp
//
//  Created by Federico Rotoli on 18/02/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var titleContainer: UIView!
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var personLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
