//
//  ClientCollectionViewCell.swift
//  PersonalTrainerApp
//
//  Created by Federico Rotoli on 20/02/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import UIKit

class ClientCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
